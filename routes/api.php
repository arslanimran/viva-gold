<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

 Route::group(['middleware' => ['auth:api']], function () {

 });
     //Banner
    Route::get('/banner', 'BannerController@get');
    Route::put('/banner', 'BannerController@update');
    Route::get('/slider', 'SliderController@get');
    Route::put('/slider', 'SliderController@update');

    // About
    Route::get('/about', 'AboutController@get');
    Route::put('/about', 'AboutController@updatedescription');

    //Video Links
    Route::get('/videolinks', 'AboutController@getvideolinks');
    Route::put('/videolinks', 'AboutController@updatevideolinks');




//Features
    Route::get('/feature', 'FeatureController@get');
    Route::post('/feature', 'FeatureController@insert');
    Route::post('/deletefeature', 'FeatureController@delete');
    Route::put('/feature', 'FeatureController@update');


    //News
    Route::get('/news', 'NewsController@get');
    Route::post('/news', 'NewsController@insert');
    Route::post('/deletenews', 'NewsController@delete');
    Route::put('/news', 'NewsController@update');

    //contact
    Route::get('/contact', 'ContactController@get');
    Route::put('/contact', 'ContactController@update');

    //Team
//    Route::get('/feature_icons', 'TeamController@get');
//    Route::post('/feature_icons', 'TeamController@insert');
//    Route::put('/feature_icons', 'TeamController@update');
//    Route::post('/deleteteammember', 'TeamController@delete');
//    Route::get('/teamorder', 'TeamController@getorders');

    //Faq
    Route::get('/faq', 'FaqController@get');
    Route::post('/faq', 'FaqController@insert');
    Route::put('/faq', 'FaqController@update');
    Route::post('/deletefaq', 'FaqController@delete');


    //Subscribe
    Route::get('/subscribe', 'SubscribeController@get');

    //Profile
    Route::get('/profile', 'ProfileController@get');

// });
