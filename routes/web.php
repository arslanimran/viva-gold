<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();


// Authentication Routes...
$this->get('admin-login', 'Auth\LoginController@showLoginForm')->name('admin-login');
$this->post('admin-login', 'Auth\LoginController@login');


Route::post('/subscribe', 'SubscribeController@subscribe')->name('subscribe');
Route::get('news-single/{id}', 'NewsController@index')->name('news-single');
Route::post('/contactUs', 'SubscribeController@contactUs')->name('contact-us');
Route::get('/get/feature_icons/{id}', 'TeamController@showmodal');
Route::get('/', function () {
    return view('landingpage');
})->name('main');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/admin-main-vivagold', 'HomeController@index')->name('admin');
    Route::get('/admin-main-vivagold/about', 'HomeController@index')->name('admin-about');
    Route::get('/admin-main-vivagold/news', 'HomeController@index')->name('admin-news');
    Route::get('/admin-main-vivagold/feature', 'HomeController@index')->name('admin-features');
    Route::get('/admin-main-vivagold/faq', 'HomeController@index')->name('admin-faq');
    Route::get('/admin-main-vivagold/contact', 'HomeController@index')->name('admin-contact');
//    Route::get('/admin-main-vivagold/feature_icons', 'HomeController@index')->name('admin-feature_icons');
    Route::get('/admin-main-vivagold/subscribe', 'HomeController@index')->name('admin-subscribe');
    Route::get('/admin-main-vivagold/profile', 'HomeController@index')->name('admin-profile');
    Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

});



