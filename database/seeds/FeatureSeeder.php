<?php

use Illuminate\Database\Seeder;

class FeatureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('features')->insert([
            'image'=> "1.png",
            'description' => "Backed by physical gold, immune from counterfeit attempts, and secured with blockchain vivagold is going to be the most secure." ,
            'slug'=>'vivagold'
        ]);

        DB::table('features')->insert([
            'image'=> "2.png",
            'description' => "2 Backed by physical gold, immune from counterfeit attempts, and secured with blockchain vivagold is going to be the most secure." ,
            'slug'=>'vivagold'
        ]);

        DB::table('features')->insert([
            'image'=> "3.png",
            'description' => "3 Backed by physical gold, immune from counterfeit attempts, and secured with blockchain vivagold is going to be the most secure." ,
            'slug'=>'vivagold'
        ]);

        DB::table('features')->insert([
            'image'=> "4.png",
            'description' => "4 Backed by physical gold, immune from counterfeit attempts, and secured with blockchain vivagold is going to be the most secure." ,
            'slug'=>'vivagold'
        ]);
        DB::table('features')->insert([
            'image'=> "5.png",
            'description' => "5 Backed by physical gold, immune from counterfeit attempts, and secured with blockchain vivagold is going to be the most secure." ,
            'slug'=>'vivagold'
        ]);

        DB::table('features')->insert([
            'image'=> "6.png",
            'description' => "6 Backed by physical gold, immune from counterfeit attempts, and secured with blockchain vivagold is going to be the most secure." ,
            'slug'=>'vivagold'
        ]);

        DB::table('features')->insert([
            'image'=> "7.png",
            'description' => "7 Backed by physical gold, immune from counterfeit attempts, and secured with blockchain vivagold is going to be the most secure." ,
            'slug'=>'vivagold'
        ]);


    }
}
