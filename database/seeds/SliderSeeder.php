<?php

use Illuminate\Database\Seeder;

class SliderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('slider1')->insert([
            'heading_slider' => "Viva",
            'description_slider' => "Registration"
        ]);
    }
}
