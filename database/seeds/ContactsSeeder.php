<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContactsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contact_links')->insert([
            "facebook" => 'https://www.facebook.com',
            'twitter' =>'https://www.twitter.com',
            'linkedIn' =>'https://www.linkedin.com',
            'github' =>'https://www.github.com',
            'pintrest' =>'https://www.pintrest.com',

        ]);
    }
}
