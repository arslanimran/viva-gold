<?php

use Illuminate\Database\Seeder;

class FaqSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //General
        DB::table('faq')->insert([
            "title" => 'What is Secure Gold Coin?',
            "category" => 'General',
            "description" => "Secure Gold Coin is a type of cryptocurrency that is asset-backed by physical gold. Cryptocurrencies are classified as a subset of digital currencies and are also classified as a subset of alternative currencies and virtual currencies.",
        ]);
        DB::table('faq')->insert([
            "title" => 'Where will gold bullion be stored?',
            "category" => 'General',
            "description" => "All Gold bullion will be stored and annually audited by Gold Silver Central Pte Limited, a Singaporean based company that specialises in buying, selling and storing gold.",
        ]);
        DB::table('faq')->insert([
            "title" => 'Why is the blockchain essential?',
            "category" => 'General',
            "description" => "Blockchain technology provides an ingenious way to securely store data and verify its integrity through a decentralised and distributed network. Blockchain inherently protects all transactions by adding each transaction to the distributed blockchain ledger so that they cannot be changed, regardless of the number of times the Secure Gold Coins are sold.",
        ]);
        DB::table('faq')->insert([
            "title" => 'Why do you need gold-backed tokens?',
            "category" => 'General',
            "description" => "Secure Gold Coins leverage the potential for gold-backed banking operations and provide token holders with a coin that is secure and trusted due to the advantages of the blockchain and the tangible value of gold bullion.",
        ]);

        DB::table('faq')->insert([
            "title" => 'What will the tokens cost?',
            "category" => 'General',
            "description" => "Secure Gold Coins will be priced at the introductory rate of approximately US$2.00 per token.",
        ]);

        //Pre-ICO

        DB::table('faq')->insert([
            "title" => 'What is Secure Gold Coin?',
            "category" => 'Pre-ICO',
            "description" => "Secure Gold Coin is a type of cryptocurrency that is asset-backed by physical gold. Cryptocurrencies are classified as a subset of digital currencies and are also classified as a subset of alternative currencies and virtual currencies.",
        ]);
        DB::table('faq')->insert([
            "title" => 'Where will gold bullion be stored?',
            "category" => 'Pre-ICO',
            "description" => "All Gold bullion will be stored and annually audited by Gold Silver Central Pte Limited, a Singaporean based company that specialises in buying, selling and storing gold.",
        ]);
        DB::table('faq')->insert([
            "title" => 'Why is the blockchain essential?',
            "category" => 'Pre-ICO',
            "description" => "Blockchain technology provides an ingenious way to securely store data and verify its integrity through a decentralised and distributed network. Blockchain inherently protects all transactions by adding each transaction to the distributed blockchain ledger so that they cannot be changed, regardless of the number of times the Secure Gold Coins are sold.",
        ]);
        DB::table('faq')->insert([
            "title" => 'Why do you need gold-backed tokens?',
            "category" => 'Pre-ICO',
            "description" => "Secure Gold Coins leverage the potential for gold-backed banking operations and provide token holders with a coin that is secure and trusted due to the advantages of the blockchain and the tangible value of gold bullion.",
        ]);

        DB::table('faq')->insert([
            "title" => 'What will the tokens cost?',
            "category" => 'Pre-ICO',
            "description" => "Secure Gold Coins will be priced at the introductory rate of approximately US$2.00 per token.",
        ]);


        //Tokens
        DB::table('faq')->insert([
            "title" => 'What is Secure Gold Coin?',
            "category" => 'Tokens',
            "description" => "Secure Gold Coin is a type of cryptocurrency that is asset-backed by physical gold. Cryptocurrencies are classified as a subset of digital currencies and are also classified as a subset of alternative currencies and virtual currencies.",
        ]);
        DB::table('faq')->insert([
            "title" => 'Where will gold bullion be stored?',
            "category" => 'Tokens',
            "description" => "All Gold bullion will be stored and annually audited by Gold Silver Central Pte Limited, a Singaporean based company that specialises in buying, selling and storing gold.",
        ]);
        DB::table('faq')->insert([
            "title" => 'Why is the blockchain essential?',
            "category" => 'Tokens',
            "description" => "Blockchain technology provides an ingenious way to securely store data and verify its integrity through a decentralised and distributed network. Blockchain inherently protects all transactions by adding each transaction to the distributed blockchain ledger so that they cannot be changed, regardless of the number of times the Secure Gold Coins are sold.",
        ]);
        DB::table('faq')->insert([
            "title" => 'Why do you need gold-backed tokens?',
            "category" => 'Tokens',
            "description" => "Secure Gold Coins leverage the potential for gold-backed banking operations and provide token holders with a coin that is secure and trusted due to the advantages of the blockchain and the tangible value of gold bullion.",
        ]);

        DB::table('faq')->insert([
            "title" => 'What will the tokens cost?',
            "category" => 'Tokens',
            "description" => "Secure Gold Coins will be priced at the introductory rate of approximately US$2.00 per token.",
        ]);



        //Client
        DB::table('faq')->insert([
            "title" => 'Collapsible Group Item #1',
            "category" => 'Client',
            "description" => "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.",
        ]);
        DB::table('faq')->insert([
            "title" => 'Collapsible Group Item #2',
            "category" => 'Client',
            "description" => "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.",
        ]);
        DB::table('faq')->insert([
            "title" => 'Collapsible Group Item #3',
            "category" => 'Client',
            "description" => "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.",
        ]);

        //Legal
        DB::table('faq')->insert([
            "title" => 'Collapsible Group Item #1',
            "category" => 'Legal',
            "description" => "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.",
        ]);
        DB::table('faq')->insert([
            "title" => 'Collapsible Group Item #2',
            "category" => 'Legal',
            "description" => "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.",
        ]);
        DB::table('faq')->insert([
            "title" => 'Collapsible Group Item #3',
            "category" => 'Legal',
            "description" => "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.",
        ]);
    }
}
