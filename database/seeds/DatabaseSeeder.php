<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(VideoLinksSeeder::class);
        $this->call(AboutSeeder::class);
        $this->call(FeatureSeeder::class);
        $this->call(FaqSeeder::class);
        $this->call(BannerSeeder::class);
        $this->call(ContactsSeeder::class);
        $this->call(TeamSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(ContactUsSeeder::class);
        $this->call(SliderSeeder::class);
        $this->call(NewsSeeder::class);
    }
}
