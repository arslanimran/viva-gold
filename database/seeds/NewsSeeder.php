<?php

use Illuminate\Database\Seeder;

class NewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('news')->insert([
            'image'=> "blog-1.jpg",
            'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries." ,
            'heading'=>'COMPANY ANNOUNCEMENT: THE ITALIAN MARKET IS OPEN AGAIN'
        ]);

        DB::table('news')->insert([
            'image'=> "blog-2.jpg",
            'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries." ,
            'heading'=>'CORPORATE ANNOUNCEMENT'
        ]);

        DB::table('news')->insert([
            'image'=> "blog-3.jpg",
            'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries." ,
            'heading'=>'ONEFOREX 10 K CHALLENGE PARTICIPANTS ARE ALREADY SETTING RECORDS!'
        ]);
    }
}
