<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AboutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('about')->insert([
            'description' => "VIVA Gold is a platform to buy educational packages. These packages come with free promotional SGC tokens that are convertible to free promotional SGC Coins. VIVA Gold is bonus based program thus one can earn bonuses from buying our packages. Earning as an VivaGold Independent Business Owner (IBO)is based solely upon the successful sale of products and services. As with any business, earnings and success at VivaGold are not guaranteed but depend primarily on the individual’s commitment, persistence and effort. 
								"
        ]);
    }
}
