<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('team')->insert([
            "name" => 'Qazi Ahmad Mukhtar',
            "title" => 'CEO & Co-Founder',
            "image" => '/dummy.png',
            "linkedIn" => 'https://www.linkedin.com',
            "twitter" => 'https://www.twitter.com',
            "category" => 'Team',
            "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",


        ]);


        DB::table('team')->insert([
            "name" => 'Rajesh Kataria',
            "title" => 'Executive Director',
            "image" => '/dummy.png',
            "linkedIn" => 'https://www.linkedin.com',
            "twitter" => 'https://www.twitter.com',
            "category" => 'Team',
            "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        ]);

        DB::table('team')->insert([
            "name" => 'Nebil Ali',
            "title" => 'Server Engineer at Computershare',
            "image" => '/dummy.png',
            "linkedIn" => 'https://www.linkedin.com',
            "twitter" => 'https://www.twitter.com',
            "category" => 'Team',
            "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        ]);


        DB::table('team')->insert([
            "name" => 'Haven Arlene Aurelio',
            "title" => 'CEO of Asia BlockChain Expo',
            "image" => '/dummy.png',
            "linkedIn" => 'https://www.linkedin.com',
            "twitter" => 'https://www.twitter.com',
            "category" => 'Team',
            "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        ]);

        DB::table('team')->insert([
            "name" => 'Zu Juri',
            "title" => 'CEO and Founder of Glocal Villager Incorporated',
            "image" => '/dummy.png',
            "linkedIn" => 'https://www.linkedin.com',
            "twitter" => 'https://www.twitter.com',
            "category" => 'Team',
            "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        ]);

        DB::table('team')->insert([
            "name" => 'Safwat Ali',
            "title" => 'Owner of Sold Real Estate & Business Agents Pty Ltd',
            "image" => '/dummy.png',
            "linkedIn" => 'https://www.linkedin.com',
            "twitter" => 'https://www.twitter.com',
            "category" => 'Team',
            "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        ]);







    }
}
