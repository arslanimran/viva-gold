<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VideoLinksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('video_links')->insert([
            'url_video_one'=> "#",
            'url_video_two' => "#"
        ]);
    }
}
