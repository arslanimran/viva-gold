<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@vivagold.io',
            'password' => bcrypt('admin@vivagold.io'),
            'api_token' => str_random(60),
            'type' => User::ADMIN_TYPE,
        ]);
    }
}

