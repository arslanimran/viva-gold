
<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>ViVA Gold</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicons -->
    <link rel="shortcut icon" href="/favicon.png">
    <link rel="shortcut icon" href="/favicon.ico">


    <!-- Google font (font-family: 'Roboto', sans-serif; Poppins ; Satisfy) -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,600,600i,700,700i,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Satisfy" rel="stylesheet">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="/landing-page/css/bootstrap.min.css">
    <link rel="stylesheet" href="/landing-page/css/plugins.css">
    <link rel="stylesheet" href="/landing-page/css/style.css">

    <!-- Cusom css -->
    <link rel="stylesheet" href="/landing-page/css/custom.css">

    <!-- Modernizer js -->
    <script src="/landing-page/js/vendor/modernizr-3.5.0.min.js"></script>
</head>
<body>
<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->


<!-- Main wrapper -->
<div class="wrapper" id="wrapper">

    <!-- Header -->
    <header id="wn__header" class="bread__header header__area header--fullwidth space-between header--one sticky__header header__absolute sticky__bg--black">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-lg-6 col-sm-6 col-6">
                    <div class="logo text-left text-left">
                        <a href="{{ route('main') }}">
                            <img src="/landing-page/images/logo.png" alt="logo images">
                        </a>
                    </div>
                </div>


            </div>
        </div>

        <!-- Mobile Menu -->
        <div class="mobile-menu d-block d-lg-none"></div>
        <!-- Mobile Menu -->
    </header>
    <!-- //Header -->
    <!-- Start Bradcaump area -->
    <div class="ht__bradcaump__area bg-image--68">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bradcaump__inner text-center">
                        <h2 class="bradcaump-title">Login Viva Gold</h2>
                    </div>

                    <section class="my_account_area section-padding--lg ">
                        <div class="container">
                            <div class="row">
                                <div class="center-div col-lg-6 col-lg-offset-3 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 ">
                                    <div class="my__account__wrapper">
                                        <div class="panel-body">
                                            @if (session('status'))
                                                <div class="alert alert-success">
                                                    {{ session('status') }}
                                                </div>
                                            @endif

                                            <form method="POST" action="{{ route('password.email') }}">
                                                {{ csrf_field() }}
                                            <div class="account__form bg-login">
                                                <div class="input__box{{ $errors->has('email') ? ' has-error' : '' }}">
                                                    <label>Email<span>*</span></label>
                                                    <input type="email" class="form-input" name="email" placeholder="Email" required>
                                                        @if ($errors->has('email'))
                                                            <span class="help-block" style="color: #fff;">
                                                                <strong>{{ $errors->first('email') }}</strong>
                                                            </span>
                                                        @endif
                                                </div>


                                                <div class="form__btn">
                                                    <button type="submit">Send Password Reset Link</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- //Main wrapper -->
<script src="/landing-page/js/vendor/jquery-3.2.1.min.js"></script>
<script src="/landing-page/js/popper.min.js"></script>
<script src="/landing-page/js/bootstrap.min.js"></script>
<script src="/landing-page/js/plugins.js"></script>
<script src="/landing-page/js/active.js"></script>

</body>
</html>
