
<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{config('app.name')}}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicons -->
    <link rel="shortcut icon" href="/favicon.png">
    <link rel="shortcut icon" href="/favicon.ico">


    <!-- Google font (font-family: 'Roboto', sans-serif; Poppins ; Satisfy) -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,600,600i,700,700i,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Satisfy" rel="stylesheet">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="/landing-page/css/bootstrap.min.css">
    <link rel="stylesheet" href="/landing-page/css/plugins.css">
    <link rel="stylesheet" href="/landing-page/css/style.css">

    <!-- Cusom css -->
    <link rel="stylesheet" href="/landing-page/css/custom.css">

    <!-- Modernizer js -->
    <script src="/landing-page/js/vendor/modernizr-3.5.0.min.js"></script>
</head>
<body>
<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->


<!-- Main wrapper -->
<div class="wrapper" id="wrapper" style="background-image: url(/landing-page/images/main-bg.jpg); !important;">

    <!-- Header -->
    <header id="wn__header"  class="bread__header header__area header--fullwidth space-between header--one sticky__header header__absolute sticky__bg--black">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-lg-6 col-sm-6 col-6">
                    <div class="logo text-left text-left">
                        <a href="{{ route('main') }}">
                            <img src="/landing-page/images/logo.png" alt="logo images">
                        </a>
                    </div>
                </div>


            </div>
        </div>

        <!-- Mobile Menu -->
        <div class="mobile-menu d-block d-lg-none"></div>
        <!-- Mobile Menu -->
    </header>
    <!-- //Header -->
    <!-- Start Bradcaump area -->
    <div class="ht__bradcaump__area bg-image--68">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 ">
                    <div class="bradcaump__inner text-center">
                        <h2 class="bradcaump-title">Login Viva Gold</h2>
                    </div>

                    <section class="my_account_area section-padding--lg " >
                        <div class="container">
                            <div class="row">
                                <div class="center-div col-lg-6 col-lg-offset-3 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 margin-auto">
                                    <div class="my__account__wrapper">
                                        <form  method="POST" action="{{ route('admin-login') }}" aria-label="{{ __('Login') }}">
                                            {{ csrf_field() }}
                                            <div class="account__form bg-login">
                                                <div class="input__box">
                                                    <label style="color: #fff !important;">Username or email address <span>*</span></label>



                                                    <input id="email" type="email" placeholder="Your Email" class="form-input{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                                    @if ($errors->has('email'))
                                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="input__box">
                                                    <label style="color: #fff !important;">Password<span>*</span></label>
                                                    <input id="password" type="password" placeholder="Your Password"
                                                           class="form-input{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                                           name="password" required>

                                                    @if ($errors->has('password'))
                                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>

                                                <div class="form__btn">
                                                    <button style="color: #fff !important;  border: 2px solid #f8f9fa !important;" type="submit">{{ __('Login') }}</button>
                                                    <label class="label-for-checkbox">
                                                        <input class="input-checkbox" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                                        <span style="color: #fff !important;">Remember me</span>
                                                    </label>
                                                </div>
                                                {{--<a class="forget_pass" href="#">Lost your password?</a>--}}
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- //Main wrapper -->
<script src="/landing-page/js/vendor/jquery-3.2.1.min.js"></script>
<script src="/landing-page/js/popper.min.js"></script>
<script src="/landing-page/js/bootstrap.min.js"></script>
<script src="/landing-page/js/plugins.js"></script>
<script src="/landing-page/js/active.js"></script>

</body>
</html>
