@php

    $team = \App\Team::all();
@endphp
<!-- Team -->
<section id="team" class="team section-padding bg-color ">
    <div class="container-fluid">
        <div class="container">
            <div class="heading text-center">
                <h6 class="sub-title animated" data-animation="fadeInUpShorter" data-animation-delay="0.2s">Team</h6>
                <h2 class="title animated" data-animation="fadeInUpShorter" data-animation-delay="0.3s">Creative vivagold <strong>Team</strong></h2>
                <div class="separator animated" data-animation="fadeInUpShorter" data-animation-delay="0.3s">
                    <span class="large"></span>
                    <span class="medium"></span>
                    <span class="small"></span>
                </div>
                <p class="content-desc animated" data-animation="fadeInUpShorter" data-animation-delay="0.4s"> vivagold Team Member & Advisors</p>
            </div>

            <div class="team-profile mt-5">
                <div class="row mb-5">
                    @foreach($team as $teams)
                        @if($teams->category == "feature_icons")
                             <div class="col-sm-12 col-md-6 col-lg-4 mb-5 animated" data-animation="jello" data-animation-delay="0.8s">
                            <div class="d-flex team-member" data-action="{{ $teams->id }}">
                                <div class="team-img float-left mr-3"  >
                                    <a  href="javascript:">
                                         <img src="/team/{{$teams->image}}" alt="team-profile-1" class="rounded-circle" width="128">
                                    </a>

                                </div>
                                <div class="profile align-self-center">
                                    <div class="name">{{ $teams->name }}</div>
                                    <div class="role">{{ $teams->title }}</div>
                                    <div class="social-profile mt-3">
                                        {{--<a  onclick="return false" href="{{$teams->linkedIn}}" target="_blank" title="Linkedin" class="mr-2"><i class="ti-linkedin"></i></a>--}}
                                        {{--<a  onclick="return false" href="{{$teams->twitter}}" target="_blank" title="Twitter" class="mr-2"><i class="ti-twitter-alt"></i></a>   <a  onclick="return false" href="{{$teams->linkedIn}}" target="_blank" title="Linkedin" class="mr-2"><i class="ti-linkedin"></i></a>--}}
                                        <a  onclick="return false" href="#" target="_blank" title="Twitter" class="mr-2"><i class="ti-twitter-alt"></i></a>
                                        <a  onclick="return false" href="#" target="_blank" title="Twitter" class="mr-2"><i class="ti-twitter-alt"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>


@section('js')
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script>


        $(document).on('cfeature_icons', '.team-img', function () {
            var $e = $(this);
            var urlfeature_icons/get/team/' + $e.pfeature_iconsts('.team-member').data('action');
            axios.get(url).then(function (r) {
                if (r.data != '') {
                    $('#teamUser12 .modal-body').html(r.data);
                    $('#teamUser12').modal('show');
                }

            }).catch(function (r) {
            })
        });



    </script>
@endsection
<!--/ Team -->

<div class="modal team-modal fade" id="teamUser12" tabindex="-1" role="dialog" aria-hidden="true">

    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>


{{--@include('subpages.teamModal')--}}


