
<section class="wn__our__story section-padding--lg bg--white" id ="education">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3 margin-auto">
                <div class="story__content text-center">
                    <h2>Education</h2>

                    <ul class="ul-edu">
                        <li>Accredited by PIOT and recognised by the ASQA(Australian Skills Quality Authority)</li>
                        <li>Online, interactive, global accessibility</li>
                        <li>Opens path way to higher education</li>
                        <li>Accredited and non-accredited courses</li>

                    </ul>

                    <div class="story__logo">
                        <a href="#">
                            <img src="/landing-page/new/logo-edu.png" alt="viva_logo">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>