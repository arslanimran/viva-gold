@php
    use App\Banner;
    $banner = Banner::all();
    $slider = \App\Slider::all();
@endphp

<!-- Start Slider area -->
<section id="home">
    <div class="slider-area slider--four slide__activation slide__arrow01 owl-carousel owl-theme">
        <!-- Start Single Slide -->
        <div class="slide animation__style04 bg-image--7 fullscreen align__center--left">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="slider__content">
                            <div class="contentbox">

                                <h2>{{ $banner[0]->heading }}</h2>

                                <h1><span class="theme-color">Gold</span> <br><p style="font-size: 30px !important; margin-top: 10px !important;">{{ $banner[0]->description }} </p></h1>
                                <div class="d-inline-block">
                                    <a class="btn-reg wow fadeInRight d-inline" data-wow-delay="1.5s"  target="_blank" href="https://vivagold.club/backoffice/login">IBO Login</a>

                                    {{--<a class="btn-reg wow fadeInRight d-inline" data-wow-delay="1.5s" target="_blank" href="https://www.securedgoldcoin.com/merchant-register">Merchant Register</a>--}}

                                    {{--<a class="btn-reg wow fadeInRight d-inline" data-wow-delay="1.5s" target="_blank" href="http://piot.edu.au/">Education Register</a><br><br>--}}

                                    {{--<a class="btn-reg wow fadeInRight d-inline" data-wow-delay="1.5s" target="_blank" href="http://18.224.249.232/">How to create SGC Wallet</a>--}}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Single Slide -->
        <!-- Start Single Slide -->
        <div class="slide animation__style04 bg-image--7 fullscreen align__center--left">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="slider__content">
                            <div class="contentbox">

                                <h2>{{ $slider[0]->heading_slider }}</h2>
                                <h1><span class="theme-color">Gold</span> <br><p style="font-size: 30px !important; margin-top: 10px !important;">{{ $slider[0]->description_slider }}</p> </h1>
                                <div class="d-inline-block">
                                    <a class="btn-reg wow fadeInRight d-inline" data-wow-delay="1.5s"  target="_blank" href="https://vivagold.club/backoffice/login">IBO Register</a>
                                    <a class="btn-reg wow fadeInRight d-inline" data-wow-delay="1.5s" target="_blank" href="https://vivagold.club/backoffice/login">Merchant Register</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Single Slide -->
    </div>
</section>
<!-- End Slider area -->
