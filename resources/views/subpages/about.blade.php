@php
    $about = \App\About::all();
    $videolinks= \App\VideoLinks::all();

@endphp

<div class="clearfix"></div>
<div class="clearfix"></div>
<!-- Start Our Story Area -->
<section id="about" class="wn__our__story section-padding--lg bg--white">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="story__content text-center">
                    <h2>How VIVA Gold Works</h2>
                    <p>{{$about[0]->description}}</p>

                    <div class="story__logo">
                        <a href="#">
                            <img src="/landing-page/images/viva-about.png" alt="viva_logo">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Our Story Area -->


<section class="add__banner__area bg--white pb--50">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                <div class="add__banner">
                    <div class="banner__thumb">
                        <a target="_blank" href="{{$videolinks[0]->url_video_one}}">
                            <img src="/landing-page/images/ad-1.jpg" alt="banner images">
                        </a>
                    </div>
                    <div class="banner__inner">

                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-12 xs-mt-30">
                <div class="add__banner">
                    <div class="banner__thumb">
                        <a target="_blank" href="{{$videolinks[0]->url_video_two}}">
                            <img src="/landing-page/images/ad-2.jpg" alt="banner images">
                        </a>
                    </div>
                    <div class="banner__inner">

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End About Us -->



<!-- Start BEst Seller Area -->
<section class="wn__bestseller__area bg--white ptb--100">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">

            </div>
        </div>
    </div>
</section>
