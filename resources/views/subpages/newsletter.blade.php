<section class="wn__newsletter__area bg__cat--3">
    <div class="container">
        <div class="row newsletter--bg newsletter--bg--2 xs-mt--30 xs-pb-0">
            <div class="col-lg-7 offset-lg-5">
                <div class="section__title text-center">
                    <h2>Sign up to newsletter</h2>
                </div>
                <div class="newsletter__block">
                    <p>Subscribe to our newsletters now and stay up-to-date with new collections</p>
                    <form action="{{ route('subscribe') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="newsletter__box">
                            <input type="email" name="email" id="email" required placeholder="Enter your e-mail">
                            <button type="submit">Subscribe</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>


