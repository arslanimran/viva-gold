@php
 $features=\App\Feature::all();
@endphp

{{--Features Start--}}

<!-- Start Instagram Area -->
<section id="packages" class="wn__instagram__block__home bg-image--9">
    <div class="instagram__wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section__title text-center">
                        <h2 style="color: #fff;">Educational Packages</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="instagram_gallery">
            <div class="instagram__item instagram__activation owl-carousel owl-theme">
                <div class="item">
                    <a class="fancybox" href="/landing-page/new/gold-pac-1.png"><img src="/landing-page/new/gold-pac-1.png" alt="Token images"></a>
                </div>
                <div class="item">
                    <a class="fancybox" href="/landing-page/new/gold-pac-2.png"><img src="/landing-page/new/gold-pac-2.png" alt="instagram images"></a>
                </div>
                <div class="item">
                    <a class="fancybox" href="/landing-page/new/gold-pac-3.png"><img src="/landing-page/new/gold-pac-3.png" alt="instagram images"></a>
                </div>
                <div class="item">
                    <a class="fancybox" href="/landing-page/new/gold-pac-4.png"><img src="/landing-page/new/gold-pac-4.png" alt="instagram images"></a>
                </div>
                <div class="item">
                    <a class="fancybox" href="/landing-page/new/gold-pac-5.png"><img src="/landing-page/new/gold-pac-5.png" alt="instagram images"></a>
                </div>
                <div class="item">
                    <a class="fancybox" href="/landing-page/new/gold-pac-6.png"><img src="/landing-page/new/gold-pac-6.png" alt="instagram images"></a>
                </div>

            </div>

            <div class="text-center" style="color:#fff; margin-top: 20px;">
                Registration fee  €25 per account will be applied at first time.
            </div>



        </div>
        <div class="taxt__bottom">
            <span>PACKAGES</span>
        </div>
    </div>
</section>
<!-- End Instagram Area -->

{{--<section class="wn__testimonial__area section-padding--lg bg__cat--1" style="background-image: url(/landing-page/images/7.jpg);" id="features">--}}
    {{--<div class="container">--}}
        {{--<div class="row">--}}

            {{--<div class="col-lg-12 col-12">--}}
                {{--<div class="testimonial__container text-center">--}}

                    {{--<div class="tes__img__slide thumb_active">--}}
                        {{--@foreach($features as $images)--}}
                        {{--<div class="testimonial__img">--}}
                            {{--<span><img src="/feature_icons/{{$images->image}}" alt="{{$images->slug}}"></span>--}}
                        {{--</div>--}}
                        {{--@endforeach--}}

                    {{--</div>--}}

                    {{--<div class="testimonial__text__slide testext_active">--}}
                        {{--@foreach($features as $feature)--}}
                        {{--<div class="clint__info">--}}

                            {{--<p style="color: #fff !important; opacity:1 !important;">{{$feature->description}}</p>--}}
                            {{--<div class="name__post">--}}
                                {{--<span>{{$feature->slug}}</span>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--@endforeach--}}

                    {{--</div>--}}


                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</section>--}}

{{--Features End--}}



