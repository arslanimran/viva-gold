{{--Footer Start--}}
<footer id="wn__footer" class="footer__area footer--news">
    <div class="footer-static-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer__widget footer__menu">
                        <div class="ft__logo">
                            <a href="{{ route('main') }}">
                                <img src="/landing-page/images/viva-bc.png" alt="logo">
                            </a>
                        </div>
                        <div class="footer__content">

                            <ul class="social__net mt--30 social__net--2 d-flex justify-content-center">
                                <li><a href="#"><i class="zmdi zmdi-rss"></i></a></li>
                                <li><a href="#"><i class="zmdi zmdi-linkedin"></i></a></li>
                                <li><a href="#"><i class="zmdi zmdi-vimeo"></i></a></li>
                                <li><a href="#"><i class="zmdi zmdi-tumblr"></i></a></li>
                                <li><a href="#"><i class="zmdi zmdi-google-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright__wrapper bg--white">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="copyright">
                        <div class="copy__right__inner text-center">
                            <p>Copyright <i class="fa fa-copyright"></i> <a href="{{ route('main') }}">VIVA Gold.</a> All Rights Reserved</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- //Footer Area -->
