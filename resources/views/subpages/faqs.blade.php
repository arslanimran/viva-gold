@php

    $faq = \App\Faq::all();
@endphp
<!-- FAQ -->
<section id="faq" class="faq section-padding bg-color faq-bg"
         style="background-image: url(/landing-page/theme-assets/images/bitcoin-banner.jpg); background-size: cover;">
    <div class="container-fluid">
        <div class="container">
            <div class="heading text-center">
                <h6 class="sub-title animated white-color" data-animation="fadeInUpShorter" data-animation-delay="0.2s">
                    FAQ</h6>
                <h2 class="title animated white-color" data-animation="fadeInUpShorter" data-animation-delay="0.3s">
                    Frequently Asked <strong>Questions</strong></h2>
                <div class="separator animated" data-animation="fadeInUpShorter" data-animation-delay="0.3s">
                    <span class="large"></span>
                    <span class="medium"></span>
                    <span class="small"></span>
                </div>
                <p class="content-desc animated white-color" data-animation="fadeInUpShorter"
                   data-animation-delay="0.4s">Originally the term "FAQ" referred to the Frequently Asked Question
                    itself, and the
                    <br/>compilation of questions and answers was known as a "FAQ list" or some similar expression.</p>
            </div>
            <div class="row">
                <div class="col">
                    <nav>
                        <div class="nav nav-pills nav-underline mb-5 animated white-color"
                             data-animation="fadeInUpShorter" data-animation-delay="0.5s" id="myTab" role="tablist">
                            <a href="#general" class="nav-item nav-link active white-color" id="general-tab"
                               data-toggle="tab" aria-controls="general" aria-selected="true" role="tab">General</a>
                            <a href="#ico" class="nav-item nav-link white-color" id="ico-tab" data-toggle="tab"
                               aria-controls="ico" aria-selected="false" role="tab">Pre-ICO</a>
                            <a href="#token" class="nav-item nav-link white-color" id="token-tab" data-toggle="tab"
                               aria-controls="token" aria-selected="false" role="tab">Tokens</a>
                            <a href="#client" class="nav-item nav-link white-color" id="client-tab" data-toggle="tab"
                               aria-controls="client" aria-selected="false" role="tab">Client</a>
                            <a href="#legal" class="nav-item nav-link white-color" id="legal-tab" data-toggle="tab"
                               aria-controls="legal" aria-selected="false" role="tab">Legal</a>
                        </div>
                    </nav>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="general" role="tabpanel"
                             aria-labelledby="general-tab">
                            <div id="general-accordion" class="collapse-icon accordion-icon-rotate">
                                @foreach($faq as $faqs)
                                    @if($faqs->category == "General")
                                        <div class="card animated" data-animation="fadeInUpShorter"
                                             data-animation-delay="{{ 0.0 + 0.1 }}s">
                                            <div class="card-header" id="heading{{$loop->index}}">
                                                <h5 class="mb-0">
                                                    <a class="btn-link" data-toggle="collapse"
                                                       data-target="#collapse{{$loop->index}}" aria-expanded="false"
                                                       aria-controls="collapse{{$loop->index}}">
                                                        <span class="icon"></span>
                                                        {{ $faqs->title }}
                                                    </a>
                                                </h5>
                                            </div>
                                            <div id="collapse{{$loop->index}}" class="collapse"
                                                 aria-labelledby="heading{{$loop->index}}"
                                                 data-parent="#general-accordion">
                                                <div class="card-body">
                                                    {{ $faqs->description }}
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        <div class="tab-pane fade" id="ico" role="tabpanel" aria-labelledby="ico-tab">
                            <div id="ico-accordion" class="collapse-icon accordion-icon-rotate">
                                @foreach($faq as $faqs)
                                    @if($faqs->category == "Pre-ICO")
                                        <div class="card">
                                            <div class="card-header" id="icoHeading{{ $loop->index }}">
                                                <h5 class="mb-0">
                                                    <a class="btn-link" data-toggle="collapse"
                                                       data-target="#icoCollapse{{ $loop->index }}" aria-expanded="true"
                                                       aria-controls="icoCollapse{{ $loop->index }}">
                                                        <span class="icon"></span>
                                                        {{ $faqs->title }}
                                                    </a>
                                                </h5>
                                            </div>
                                            <div id="icoCollapse{{ $loop->index }}" class="collapse"
                                                 aria-labelledby="icoHeading{{ $loop->index }}"
                                                 data-parent="#ico-accordion">
                                                <div class="card-body">
                                                    {{ $faqs->description }}
                                                </div>

                                            </div>
                                        </div>
                                    @endif
                                @endforeach

                            </div>
                        </div>
                        <div class="tab-pane fade" id="token" role="tabpanel" aria-labelledby="token-tab">
                            <div id="token-accordion" class="collapse-icon accordion-icon-rotate">
                                @foreach($faq as $faqs)
                                    @if($faqs->category == "Tokens")
                                        <div class="card">
                                            <div class="card-header" id="tokenHeading{{ $loop->index }}">
                                                <h5 class="mb-0">
                                                    <a class="btn-link" data-toggle="collapse"
                                                       data-target="#tokenCollapse{{ $loop->index }}"
                                                       aria-expanded="true"
                                                       aria-controls="tokenCollapse{{ $loop->index }}">
                                                        <span class="icon"></span>
                                                        {{ $faqs->title }}
                                                    </a>
                                                </h5>
                                            </div>
                                            <div id="tokenCollapse{{ $loop->index }}" class="collapse"
                                                 aria-labelledby="tokenHeading{{ $loop->index }}"
                                                 data-parent="#token-accordion">
                                                <div class="card-body">
                                                    {{ $faqs->description }}
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        <div class="tab-pane fade" id="client" role="tabpanel" aria-labelledby="client-tab">
                            <div id="client-accordion" class="collapse-icon accordion-icon-rotate">
                                @foreach($faq as $faqs)
                                    @if($faqs->category == "Client")
                                        <div class="card">
                                            <div class="card-header" id="clientHeading{{ $loop->index }}">
                                                <h5 class="mb-0">
                                                    <a class="btn-link" data-toggle="collapse"
                                                       data-target="#clientCollapse{{ $loop->index }}"
                                                       aria-expanded="true"
                                                       aria-controls="clientCollapse{{ $loop->index }}">
                                                        <span class="icon"></span>
                                                        {{ $faqs->title  }}
                                                    </a>
                                                </h5>
                                            </div>
                                            <div id="clientCollapse{{ $loop->index }}" class="collapse"
                                                 aria-labelledby="clientHeading{{ $loop->index }}"
                                                 data-parent="#client-accordion">
                                                <div class="card-body">
                                                    {{ $faqs->description }}
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        <div class="tab-pane fade" id="legal" role="tabpanel" aria-labelledby="legal-tab">
                            <div id="legal-accordion" class="collapse-icon accordion-icon-rotate">
                                @foreach($faq as $faqs)
                                    @if($faqs->category == "Legal")
                                        <div class="card">
                                            <div class="card-header" id="legalHeading{{ $loop->index }}">
                                                <h5 class="mb-0">
                                                    <a class="btn-link" data-toggle="collapse"
                                                       data-target="#legalCollapse{{ $loop->index }}" aria-expanded="true"
                                                       aria-controls="legalCollapse{{ $loop->index }}">
                                                        <span class="icon"></span>
                                                        {{ $faqs->title }}
                                                    </a>
                                                </h5>
                                            </div>
                                            <div id="legalCollapse{{ $loop->index }}" class="collapse"
                                                 aria-labelledby="legalHeading{{ $loop->index }}" data-parent="#legal-accordion">
                                                <div class="card-body">
                                                   {{ $faqs->description }}
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ FAQ -->
