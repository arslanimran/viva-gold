

<!-- Header -->
<header id="wn__header" class="header__area header--fullwidth space-between header--four header__absolute sticky__bg--black sticky__header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-6 col-lg-3">
                <div class="logo">
                    <a href="{{ route('main') }}">
                        <img src="/landing-page/images/logo.png" alt="logo images">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- Start Mobile Menu -->

    <!-- End Mobile Menu -->
    <div class="mobile-menu d-block d-lg-none">
    </div>
    <!-- Mobile Menu -->
</header>
<!-- //Header -->


