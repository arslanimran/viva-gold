<section class="wn__instagram__block__home bg-image--9" id="opportunity">
    <div class="instagram__wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="instagram">
                        <div class="title_instagram">
                            <h2><span class="theme-color">A POWERFUL</span> OPPORTUNITY</h2>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="instagram_gallery">
            <div class="instagram__item instagram__activation owl-carousel owl-theme">
                <div class="item">
                    <a class="fancybox" href="/landing-page/images/token/1.png"><img src="/landing-page/images/token/1.png" alt="Token images"></a>
                </div>
                <div class="item">
                    <a class="fancybox" href="/landing-page/images/token/2.png"><img src="/landing-page/images/token/2.png" alt="instagram images"></a>
                </div>
                <div class="item">
                    <a class="fancybox" href="/landing-page/images/token/3.png"><img src="/landing-page/images/token/3.png" alt="instagram images"></a>
                </div>
                <div class="item">
                    <a class="fancybox" href="/landing-page/images/token/4.png"><img src="/landing-page/images/token/4.png" alt="instagram images"></a>
                </div>
                <div class="item">
                    <a class="fancybox" href="/landing-page/images/token/5.png"><img src="/landing-page/images/token/5.png" alt="instagram images"></a>
                </div>
                <div class="item">
                    <a class="fancybox" href="/landing-page/images/token/6.png"><img src="/landing-page/images/token/6.png" alt="instagram images"></a>
                </div>
                <div class="item">
                    <a class="fancybox" href="/landing-page/images/token/7.png"><img src="/landing-page/images/token/7.png" alt="instagram images"></a>
                </div>
                <div class="item">
                    <a class="fancybox" href="/landing-page/images/token/1.png"><img src="/landing-page/images/token/1.png" alt="instagram images"></a>
                </div>
                <div class="item">
                    <a class="fancybox" href="/landing-page/images/token/2.png"><img src="/landing-page/images/token/2.png" alt="instagram images"></a>
                </div>
                <div class="item">
                    <a class="fancybox" href="/landing-page/images/token/3.png"><img src="/landing-page/images/token/3.png" alt="instagram images"></a>
                </div>
            </div>
        </div>
        <div class="taxt__bottom">
            <span>Coin & Tokens</span>
        </div>
    </div>
</section>
