@php
    use App\Contacts;
    $contacts = Contacts::all();
@endphp
<!-- Contact -->


<section class="wn__banner__static bg--white section-padding--lg" style="padding-bottom: 0px;" id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="contact-form-wrap">
                    <div class="section__title text-center">
                        <h2>Get in touch</h2>
                    </div>

                    {{--<p class="text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,  </p>--}}
                    <form  action="{{ route('contact-us') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="single-contact-form space-between">
                            <input type="text" name="name"  id ="name" placeholder="Name*">
                            <input type="email" name="email" id="email" placeholder="Email*">
                        </div>
                        <div class="single-contact-form message">
                            <textarea name="message" id="message"  placeholder="Type your message here."></textarea>
                        </div>
                        <div class="contact-btn text-center">
                            <button type="submit">Send Email</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>



    <footer id="wn__footer" class="footer__area footer--four">
        <div class="footer-static-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer__widget footer__menu">
                            <div class="ft__logo">
                                <a href="{{ route('main') }}">
                                    <img src="/landing-page/images/viva-bc.png" alt="logo">
                                </a>
                            </div>
                            <div class="footer__content">
                                <ul class="mainmenu d-flex justify-content-center">
                                    <li><a href="#home">Home</a></li>
                                    <li><a href="#about">About</a></li>
                                    <li><a href="#packages">Packages</a></li>
                                    <li><a href="#education">Education</a></li>
                                    <li><a href="#news">News</a></li>
                                    <li><a href="#contact">Contact</a></li>
                                </ul>
                                <ul class="social__net mt--30 social__net--2 d-flex justify-content-center">
                                    <li><a href="#"><i class="zmdi zmdi-rss"></i></a></li>
                                    <li><a href="#"><i class="zmdi zmdi-linkedin"></i></a></li>
                                    <li><a href="#"><i class="zmdi zmdi-vimeo"></i></a></li>
                                    <li><a href="#"><i class="zmdi zmdi-tumblr"></i></a></li>
                                    <li><a href="#"><i class="zmdi zmdi-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright__wrapper bg--white">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="copyright">
                                    <div class="copy__right__inner text-center">
                                        <a href="#">Terms & Conditions</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="copyright">
                                    <div class="copy__right__inner text-center">
                                        <p>Copyright <i class="fa fa-copyright"></i> <a href="{{ route('main') }}">VIVA Gold.</a> All Rights Reserved</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="copyright">
                                    <div class="copy__right__inner text-center">
                                        <a href="#">Privacy Policy</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </footer>
    <!-- //Footer Area -->
