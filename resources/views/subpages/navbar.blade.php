<!-- Header -->
<header id="wn__header" class="header__area header--fullwidth space-between header--four header__absolute sticky__bg--black sticky__header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-6 col-lg-3">
                <div class="logo">
                    <a href="{{ route('main') }}">
                        <img src="/landing-page/images/logo.png" alt="logo images">
                    </a>
                </div>
            </div>
            <div class="col-lg-6 d-none d-lg-block">
                <nav class="mainmenu__nav">
                    <ul class="meninmenu d-flex justify-content-center">
                        <li class="drop with--one--item"><a href="#home">Home</a>
                        </li>
                        <li class="drop"><a href="#about">About</a>
                        </li>

                        <li class="drop"><a href="#packages">Packages</a>
                        </li>
                        <li class="drop"><a href="#education">Education</a>
                        </li>
                        <li class="drop"><a href="#news">News</a>
                        </li>
                        <li><a href="#contact">Contact</a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-md-6 col-sm-6 col-6 col-lg-3">
                <ul class="header__sidebar__right d-flex justify-content-end align-items-center">
                    <li class=""><a class="nav-reg" target="_blank" href="https://vivagold.club/backoffice/login">IBO Login</a>
                    </li>
                    {{--<li class=""><a class="nav-reg" target="_blank" href="https://www.securedgoldcoin.com/merchant-register">Merchant Register</a>--}}
                    {{--</li>--}}


                </ul>
            </div>
        </div>
    </div>
    <!-- Start Mobile Menu -->
    <div class="row d-none">
        <div class="col-lg-12 d-none">
            <nav class="mobilemenu__nav">
                <ul class="meninmenu">
                    <li><a href="#home">Home</a>

                    </li>
                    <li><a href="#about">About</a>
                    </li>
                    <li><a href="#features">Features</a>

                    </li>
                    <li><a href="#news">News</a>

                    </li>
                    <li><a href="#opportunity">Opportunity</a>

                    </li>
                    <li><a href="#contact">Contact</a></li>
                </ul>
            </nav>
        </div>
    </div>
    <!-- End Mobile Menu -->
    <div class="mobile-menu d-block d-lg-none">
    </div>
</header>
<!-- //Header -->


