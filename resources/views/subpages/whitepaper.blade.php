
{{--News Section--}}
<section class="wn__banner__static bg--white section-padding--lg">
    <div class="container">
        <div class="row">

            <div class="col-lg-12 col-sm-12 col-12">
                <div class="section__title text-center">
                    <h2>LATEST NEWS</h2>
                </div>

                <br><br><br>
            </div>


            <!-- Start Single Banner -->
            <div class="col-lg-4 col-sm-6 col-12">
                <div class="hot__banner">
                    <div class="thumb">
                        <a href="#">
                            <img src="/landing-page/images/blog-1.jpg" alt="banner images">
                        </a>
                    </div>
                    <div class="banner__content">
                        <h3>COMPANY ANNOUNCEMENT: THE ITALIAN MARKET IS OPEN AGAIN</h3>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,</p>
                        <a class="shopbtn" href="#">READ FULL</a>
                    </div>
                </div>
            </div>
            <!-- End Single Banner -->
            <!-- Start Single Banner -->
            <div class="col-lg-4 col-sm-6 col-12 xs-mt-40">
                <div class="hot__banner box2">
                    <div class="banner__content">
                        <h3>CORPORATE ANNOUNCEMENT</h3>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,</p>
                        <a class="shopbtn" href="#">READ FULL</a>
                    </div>
                    <div class="thumb">
                        <a href="#">
                            <img src="/landing-page/images/blog-2.jpg" alt="banner images">
                        </a>
                    </div>
                </div>
            </div>
            <!-- End Single Banner -->
            <!-- Start Single Banner -->
            <div class="col-lg-4 col-sm-6 col-12 sm-mt-40 xs-mt-40">
                <div class="hot__banner">
                    <div class="thumb">
                        <a href="#">
                            <img src="/landing-page/images/blog-3.jpg" alt="banner images">
                        </a>
                    </div>
                    <div class="banner__content">
                        <h3>ONEFOREX 10 K CHALLENGE PARTICIPANTS ARE ALREADY SETTING RECORDS!</h3>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,</p>
                        <a class="shopbtn" href="#">READ FULL</a>
                    </div>
                </div>
            </div>
            <!-- End Single Banner -->
        </div>
    </div>
</section>
{{--News Section End--}}
