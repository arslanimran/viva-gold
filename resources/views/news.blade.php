<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{config('app.name')}}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicons -->
    <link rel="shortcut icon" href="/favicon.png">
    <link rel="shortcut icon" href="/favicon.ico">


    <!-- Google font (font-family: 'Roboto', sans-serif; Poppins ; Satisfy) -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,600,600i,700,700i,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Satisfy" rel="stylesheet">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="/landing-page/css/bootstrap.min.css">
    <link rel="stylesheet" href="/landing-page/css/plugins.css">
    <link rel="stylesheet" href="/landing-page/css/style.css">

    <!-- Cusom css -->
    <link rel="stylesheet" href="/landing-page/css/custom.css">

    <!-- Modernizer js -->
    <script src="/landing-page/js/vendor/modernizr-3.5.0.min.js"></script>
</head>
<body>

<!-- Main wrapper -->
<div class="wrapper" id="wrapper">

    @include('subpages.navbar2')


    @include('subpages.news-single')


    @include('subpages.footer')







    <!-- Start Instagram Area -->

    <!-- End Instagram Area -->
    <!-- Footer Area -->


</div>
        <!-- JS Files -->
<script src="/landing-page/js/vendor/jquery-3.2.1.min.js"></script>
<script src="/landing-page/js/popper.min.js"></script>
<script src="/landing-page/js/bootstrap.min.js"></script>
<script src="/landing-page/js/plugins.js"></script>
<script src="/landing-page/js/active.js"></script>



<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">
@yield('js')


<script>
    @if(Session::has('alert'))
    toastr.error("{{ Session::get('alert') }}");
    @endif
    @if(Session::has('success'))
    toastr.success("{{ Session::get('success') }}");
    @endif
</script>


</body>

</html>
