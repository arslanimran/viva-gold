@component('mail::message')
Welcome to {{config('app.name')}} Platfrom,<br><br>

Your subscription has been confirmed. You have been added to our list and will hear from us soon.

Regards,<br>
{{config('app.name')}} Team
@endcomponent
