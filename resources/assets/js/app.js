
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.VueRouter =  require('vue-router').default;
window.VueResource = require('vue-resource');

Vue.http.interceptors.push(function (request, next) {
    request.headers['X-CSRF-TOKEN'] = document.querySelector("meta[name='csrf-token']").attributes.content.value;
    request.headers['Authorization'] = "bearer "+document.head.querySelector('meta[name="api_token"]').attributes.content.value;
    next();
});

window.myheaders = {
    "Authorization": "bearer "+  document.querySelector("meta[name='api_token']").attributes.content.value,
    "X-Requested-With": 'XMLHttpRequest',
    'X-CSRF-TOKEN': document.querySelector("meta[name='csrf-token']").attributes.content.value,
    "Accept": "application/json",
    "cache-control": "no-cache"
  };

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

BannerComponent = Vue.component('banner-component', require('./components/MainComponent.vue'));
AboutComponent = Vue.component('banner-component', require('./components/AboutComponent.vue'));
FeaturesComponent = Vue.component('banner-component', require('./components/FeaturesComponent.vue'));
NewsComponent = Vue.component('banner-component', require('./components/NewsComponent.vue'));
ContactComponent = Vue.component('banner-component', require('./components/ContactComponent.vue'));
// TeamComponent = Vue.component('banner-component', require('./components/TeamComponent.vue'));
// FaqComponent = Vue.component('banner-component', require('./components/FaqComponent.vue'));
SubscribeComponent = Vue.component('banner-component', require('./components/SubscribeComponent.vue'));
ProfileComponent = Vue.component('banner-component', require('./components/ProfileComponent.vue'));
var router = new VueRouter(
    {
        mode: 'history',
        routes: [
            {path: '', name: 'main', component: BannerComponent},
            {path: '/admin-main-vivagold', name: 'banner',  component: BannerComponent},
            {path: '/admin-main-vivagold/about', name: 'about',  component: AboutComponent},
            {path: '/admin-main-vivagold/feature', name: 'feature',  component: FeaturesComponent},
            {path: '/admin-main-vivagold/news', name: 'news',  component: NewsComponent},
            {path: '/admin-main-vivagold/contact', name: 'contact',  component: ContactComponent},
            // {path: '/admin-main-vivagold/feature_icons', name: 'feature_icons',  component: TeamComponent},
            // {path: '/admin-main-vivagold/faq', name: 'faq',  component: FaqComponent},
            {path: '/admin-main-vivagold/subscribe', name: 'subscribe',  component: SubscribeComponent},
            {path: '/admin-main-vivagold/profile', name: 'profile',  component: ProfileComponent},
        ]
    }
);

const app = new Vue({
    el: '#app',
    router: router
});


