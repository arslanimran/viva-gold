<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $table= "slider1";
    protected $fillable= ['heading_slider', 'description_slider'];

}
