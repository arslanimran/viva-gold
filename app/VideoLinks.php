<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoLinks extends Model
{
    protected $table = 'video_links';
    protected $fillable = ['url_video_one','url_video_two'];
}
