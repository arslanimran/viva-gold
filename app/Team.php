<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $table = "feature_icons";
    protected $fillable = ['name', 'title', 'image', 'linkedIn','twitter', 'category','description'];
}
