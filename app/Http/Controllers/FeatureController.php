<?php

namespace App\Http\Controllers;

use App\Feature;
use Illuminate\Http\Request;

class FeatureController extends Controller
{

    public function get(Request $request){
        return json_encode(Feature::all());
    }

    public function insert(Request $request){
        if($request->input('image'))
            $exploded_image = explode(',',$request->input('image'));
        if($request->input('image'))
            $imagepath = public_path()."/feature_icons/".str_random().(str_contains($exploded_image[0], 'jpeg') ? '.jpg' : '.png');
        if($request->input('image'))
            file_put_contents($imagepath, base64_decode($exploded_image[1]));

        return Feature::create([

            'image' => explode('feature_icons/', $imagepath)[1],
            'description' => $request->input('description'),
            'slug' => $request->input('slug'),

        ]);
    }

    public function update(Request $request){
        if($request->input('image') != "")
            $exploded_image = explode(',',$request->input('image'));
        if($request->input('image') != "")
            $imagepath = public_path()."/feature_icons/".str_random().(str_contains($exploded_image[0], 'jpeg') ? '.jpg' : '.png');
        if($request->input('image') != "")
            file_put_contents($imagepath, base64_decode($exploded_image[1]));

//
        $feature =  Feature::findOrFail($request->input('id'));

        $feature->description = $request->input('description');
        $feature->slug = $request->input('slug');
        if($request->input('image') != ""){
            $feature->image = explode('feature_icons/', $imagepath)[1];
        }
        return json_encode($feature->save());
    }

    public function delete(Request $request){
        return json_encode(Feature::findOrFail($request->id)->delete());
    }

//    public function showmodal($id)
//    {
//        $teams = Feature::whereId($id)->first();
//
//
//        return view('subpages.teamModal',compact('teams'));
//
////        return response()->json(['teamModal'=>$teams,'status'=>'true']);
//    }
}
