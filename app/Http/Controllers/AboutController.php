<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\About;
use App\VideoLinks;
class AboutController extends Controller
{
    public function get(Request $request){
        return json_encode(About::where('id', 1)->get());
    }
    public function updatedescription(Request $request){
        $about = About::findOrFail(1);
        $about->description = $request->input('description');
        return json_encode($about->save());
    }

//    Update Video Links

    public function getvideolinks(Request $request){
        return json_encode(VideoLinks::all());
    }
    public function updatevideolinks(Request $request){
        $videolinks = VideoLinks::findOrFail(1);
        $videolinks->url_video_one = $request->input('url_video_one');
        $videolinks->url_video_two = $request->input('url_video_two');
        return json_encode($videolinks->save());
    }
}
