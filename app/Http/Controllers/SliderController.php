<?php

namespace App\Http\Controllers;

use App\Slider;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    public function update(Request $request){
        $slider = Slider::findOrFail(1);
        $slider->heading_slider = $request->input('heading_slider');
        $slider->description_slider = $request->input('description_slider');
        return json_encode($slider->save());
    }

    public function get(Request $request){
        return Slider::where('id',1)->get();
    }
}
