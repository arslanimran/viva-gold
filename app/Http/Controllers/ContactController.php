<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contacts;
class ContactController extends Controller
{
    public function update(Request $request){
        $banner = Contacts::findOrFail(1);
        $banner->facebook = $request->input('facebook');
        $banner->twitter = $request->input('twitter');
        $banner->linkedIn = $request->input('linkedIn');
        $banner->github = $request->input('github');
        $banner->pintrest = $request->input('pintrest');
        return json_encode($banner->save());
    }

    public function get(Request $request){
        return Contacts::where('id',1)->get();
    }
}
