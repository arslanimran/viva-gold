<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Team;

class TeamController extends Controller
{

    public function get(Request $request){
        return json_encode(Team::all());
    }

    public function insert(Request $request){
        if($request->input('image'))
            $exploded_image = explode(',',$request->input('image'));
        if($request->input('image'))
            $imagepath = public_path()."/feature_icons/".str_random().(str_contains($exploded_image[0], 'jpeg') ? '.jpg' : '.png');
        if($request->input('image'))
            file_put_contents($imagepath, base64_decode($exploded_image[1]));

        return Team::create([
            'name' => $request->input('name'),
            'title' => $request->input('title'),
            'image' => explode('feature_icons/', $imagepath)[1],
            'linkedIn' => $request->input('linkedIn'),
            'twitter' => $request->input('twitter'),
            'category' => $request->input('category'),
            'description' => $request->input('description')
        ]);
    }

    public function update(Request $request){
        if($request->input('image') != "")
            $exploded_image = explode(',',$request->input('image'));
        if($request->input('image') != "")
            $imagepath = public_path()."/feature_icons/".str_random().(str_contains($exploded_image[0], 'jpeg') ? '.jpg' : '.png');
        if($request->input('image') != "")
            file_put_contents($imagepath, base64_decode($exploded_image[1]));

//
        $team = Team::findOrFail($request->input('id'));
        $team->name = $request->input('name');
        $team->title = $request->input('title');
        $team->linkedIn = $request->input('linkedIn');
        $team->twitter = $request->input('twitter');
        $team->category = $request->input('category');
        $team->description = $request->input('description');
        if($request->input('image') != ""){
            $team->image = explode('feature_icons/', $imagepath)[1];
        }
        return json_encode($team->save());
    }

    public function delete(Request $request){
        return json_encode(Team::findOrFail($request->id)->delete());
    }

    public function showmodal($id)
    {
        $teams = Team::whereId($id)->first();


        return view('subpages.teamModal',compact('teams'));

//        return response()->json(['teamModal'=>$teams,'status'=>'true']);
    }


}
