<?php

namespace App\Http\Controllers;
use App\Mail\ContactUsMail;
use App\Mail\SubscribeEmail;
use App\Subscribe;
use App\ContactUs;
use App\Rules\AlphaSpace;
use Illuminate\Http\Request;

class SubscribeController extends Controller
{

    public function subscribe(Request $request)
    {

        $this->validate($request, [
            'email' =>'required|email'
        ]);

        $subscriberEmail = Subscribe::whereEmail($request['email'])->orderBy('id', 'desc')->first();


        if ($subscriberEmail)

//            return 'azeem has already subscribed';
            return redirect('/')->with('alert', 'You are already subscribed on vivagold.io');
//            return response()->json(['type' => 'warning', 'message' => 'you are already subscribed on Bartvault.io ']);

        Subscribe::create(['email' => $request->get('email')]);

        \Mail::to($request->get('email'))->send(new SubscribeEmail($request->all()));

        return redirect('/')->with('success', 'Your subscription has been added');
//        return response()->json(['message' => 'Your subscription has been added.']);

//        return 'azeem subscried successfully';

    }
    public function contactUs(Request $request)
    {
        $this->validate($request, [
            'name' => ['required', new AlphaSpace],
            'email' =>'required|email',
            'message' => 'required|string'
        ]);

        \Mail::to(ContactUs::whereName('contact_us_email')->first()->value)->send(new ContactUsMail($request->all()));

        return redirect('/')->with('success', 'Your message has been sent.');
//
//        return response()->json(['message' => 'Your message has been sent.']);
    }

    public function get(Request $request){
        return json_encode(Subscribe::all());
    }



}
