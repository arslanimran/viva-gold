<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    //
    public function get(Request $request){
        return json_encode(User::where('id','!=','1')->get());
    }
}
