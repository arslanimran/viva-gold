<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function index($id){

        $news = News::find($id);
        return view('news',compact('news'));
    }

    public function get(Request $request){
        return json_encode(News::all());
    }

    public function insert(Request $request){
        if($request->input('image'))
            $exploded_image = explode(',',$request->input('image'));
        if($request->input('image'))
            $imagepath = public_path()."/feature_icons/".str_random().(str_contains($exploded_image[0], 'jpeg') ? '.jpg' : '.png');
        if($request->input('image'))
            file_put_contents($imagepath, base64_decode($exploded_image[1]));

        return News::create([

            'image' => explode('feature_icons/', $imagepath)[1],
            'description' => $request->input('description'),
            'heading' => $request->input('heading'),

        ]);
    }

    public function update(Request $request){
        if($request->input('image') != "")
            $exploded_image = explode(',',$request->input('image'));
        if($request->input('image') != "")
            $imagepath = public_path()."/feature_icons/".str_random().(str_contains($exploded_image[0], 'jpeg') ? '.jpg' : '.png');
        if($request->input('image') != "")
            file_put_contents($imagepath, base64_decode($exploded_image[1]));

//
        $news =  News::findOrFail($request->input('id'));

        $news->description = $request->input('description');
        $news->heading = $request->input('heading');
        if($request->input('image') != ""){
            $news->image = explode('feature_icons/', $imagepath)[1];
        }
        return json_encode($news->save());
    }

    public function delete(Request $request){
        return json_encode(News::findOrFail($request->id)->delete());
    }
}
