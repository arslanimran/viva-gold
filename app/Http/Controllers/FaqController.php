<?php

namespace App\Http\Controllers;

use App\Faq;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    //
    public function get(Request $request){
        return json_encode(Faq::all());
    }

    public function insert(Request $request){


        return Faq::create([

            'title' => $request->input('title'),
            'category' => $request->input('category'),
            'description' => $request->input('description')
        ]);
    }

    public function update(Request $request){

        $faq = Faq::findOrFail($request->input('id'));

        $faq->title = $request->input('title');
        $faq->category = $request->input('category');
        $faq->description = $request->input('description');

        return json_encode($faq->save());
    }

    public function delete(Request $request){
        return json_encode(Faq::findOrFail($request->id)->delete());
    }

}
